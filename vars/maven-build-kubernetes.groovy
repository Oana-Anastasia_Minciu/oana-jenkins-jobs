
podTemplate(label: "jenkins-k8s-node", cloud: "kubernetes", runAsUser: '1000', activeDeadlineSeconds: 540, hostNetwork: true, containers: [
    containerTemplate(name: 'jnlp', image: 'jenkins/inbound-agent:latest', workingDir: '/home/jenkins/', args: '${computer.jnlpmac} ${computer.name}'),
    containerTemplate(name: 'maven', image: 'maven:3.8.1-openjdk-11-slim', 	workingDir: '/home/jenkins/', ttyEnabled: true, command: 'bash')
  ]) 
  {
    def repository_url = '<url of git repository>'
    def repository_credentials = '<id of credentials from Jenkins>'
    def sonar_url = '<url of sonar server.'
    node("jenkins-k8s-node")
    {
    
        container('jnlp')
        {
            
            stage("Preparation ${serviceName}") 
            {
                if (env.CHANGE_BRANCH) {
                    git(
                        url: "${repository_url}",
                        credentialsId: repository_credentials,
                        branch: "${env.CHANGE_BRANCH}"
                    )
                } else {
                    git(
                        url: "${repository_url}",
                        credentialsId: repository_credentials
                    )
                }
            }
        }
    
        
        container('maven')
        {                
            stage ('Build')
            {
                sh 'mvn -U -B clean org.jacoco:jacoco-maven-plugin:prepare-agent verify -Dsonar.host.url=${sonar_url}'
            }
        }
    }
}