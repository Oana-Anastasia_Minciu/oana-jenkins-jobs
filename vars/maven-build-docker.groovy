
node('docker')
{
    def build_home_dir = '/home/jenkins'
   
    def system_home_dir = "/tmp/build_home"
    def docker_image = "<docker image>"
    def sonar_url = "<sonar>"
    def repository_url = "<url>"
    def repository_credentials = "<credentials id from Jenkins"
    def codeCoverage = "80"

    stage("Preparation ${serviceName}") {
        if (env.CHANGE_BRANCH) {
            git(
                    url: "${repository_url}",
                    credentialsId: repository_credentials,
                    branch: "${env.CHANGE_BRANCH}"
            )
        } else {
            git(
                    url: "${repository_url}",
                    credentialsId: repository_credentials
            )
        }
    }

    sh "mkdir -p ${system_home_dir} ${system_home_dir}/.m2"

    configFileProvider([configFile(fileId: 'global-maven-settings', targetLocation: "${system_home_dir}/.m2/settings.xml"),
                        configFile(fileId: 'npmrc', targetLocation: "${system_home_dir}/.npmrc"),
                        configFile(fileId: 'bowerrc', targetLocation: "${system_home_dir}/.bowerrc"),
                        configFile(fileId: 'gitconfig', targetLocation: "${system_home_dir}/.gitconfig")]) 
    {
        def img = docker.image("${docker_image}")
        img.pull()
        img.inside("-v ${system_home_dir}/:${build_home_dir}/") 
        {
            stage('Build') 
            {
                sh "mvn -U -B clean org.jacoco:jacoco-maven-plugin:prepare-agent verify -Dsonar.host.url=${sonar_url}"
                junit '**/target/surefire-reports/*.xml'
                jacoco(
                    changeBuildStatus: currentServiceConfig.get("enableJacoco"),
                    minimumClassCoverage: codeCoverage,
                    maximumClassCoverage: codeCoverage,
                    minimumMethodCoverage: codeCoverage,
                    maximumMethodCoverage: codeCoverage,
                    minimumLineCoverage: codeCoverage,
                    maximumLineCoverage: codeCoverage,
                    minimumBranchCoverage: codeCoverage,
                    maximumBranchCoverage: codeCoverage,
                    minimumInstructionCoverage: codeCoverage,
                    maximumInstructionCoverage: codeCoverage,
                    minimumComplexityCoverage: codeCoverage,
                    maximumComplexityCoverage: codeCoverage,
                    exclusionPattern: currentServiceConfig.get("jacocoExclusionPattern")
                )

            }

            stage('Quality Gate') 
            {
                timeout(time: 5, unit: 'MINUTES') // wait for sonar to compute the quality gate
                {
                    // Just in case something goes wrong, pipeline will be killed after a timeout
                    def qg = waitForQualityGate() 
                    if (qg.status != 'OK') 
                    {
                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
                    }
                }

            }

            // build is triggered from the project or from another project (like a jenkins-jobs project)
            if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == null || env.BRANCH_NAME == 'develop') 
            {
                    stage('Deploy to Nexus') 
                    {
                        sh "mvn -U -B -DskipTests deploy"
                    }
            }

        }

    }
}